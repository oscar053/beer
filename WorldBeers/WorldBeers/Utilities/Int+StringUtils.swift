//
//  Int+StringUtiles.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 27/01/22.
//

import Foundation

extension Int {
    var labelString: String {
        return String(format: "%d", self)
    }
}

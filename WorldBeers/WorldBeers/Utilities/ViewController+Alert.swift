//
//  ViewController+Alert.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 29/01/22.
//

import UIKit

extension UIViewController {
    
    func alert(message: String, title: String = "", okHandler: @escaping ((UIAlertAction) -> Void) = { _ in }, retryHandler: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: okHandler)
        alertController.addAction(okAction)
        
        if let retryHandler = retryHandler {
            let retryAction = UIAlertAction(title: "Retry", style: .cancel, handler: retryHandler)
            alertController.addAction(retryAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

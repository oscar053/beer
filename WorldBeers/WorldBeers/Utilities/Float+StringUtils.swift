//
//  Float+StringUtils.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 27/01/22.
//

import Foundation

extension Float {
    var labelString: String {
        return String(format: "%0.1f", self)
    }
}

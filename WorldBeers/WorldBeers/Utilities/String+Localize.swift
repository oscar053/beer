//
//  String+Localize.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 27/01/22.
//

import Foundation

extension String {
    var localized:String {
        return NSLocalizedString(self, comment: "")
    }
}

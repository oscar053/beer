//
//  BeerDetailViewController.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 27/01/22.
//

import UIKit

class BeerDetailViewController: UIViewController {
    
    @IBOutlet weak var firstBrewedLabel: UILabel!
    @IBOutlet weak var foodPairingLabel: UILabel!
    @IBOutlet weak var brewersTipsLabels: UILabel!
    
    var beer: Beer! {
        didSet {
            configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(named: "backgroundLight")
        
        configureView()
    }
    
    func configureView() {
        
        guard let beer = self.beer,
              let firstBrewedLabel = self.firstBrewedLabel,
              let foodPairingLabel = self.foodPairingLabel,
              let brewersTipsLabels = self.brewersTipsLabels else { return }
        
        title = beer.name
        
        firstBrewedLabel.text = "beer.detail.first.brewed".localized + beer.first_brewed
        
        foodPairingLabel.text = "beer.detail.food.pairing".localized
        beer.food_pairing.forEach { food in
            foodPairingLabel.text?.append(contentsOf: "\n")
            foodPairingLabel.text?.append(contentsOf: food)
        }
        
        brewersTipsLabels.text = "beer.detail.brewers.tips".localized + "\n" + beer.brewers_tips
    }
}

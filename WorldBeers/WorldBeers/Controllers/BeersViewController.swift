//
//  ViewController.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 26/01/22.
//

import UIKit
import Kingfisher

class BeersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    let spinnerController = SpinnerViewController()
    
    private let viewModel = BeersViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startSpinnerView()
        
        viewModel.filteredData.bind { [weak self] beers in
            guard let _ = beers else { return }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self?.stopSpinnerView()
                self?.tableView.reloadData()
            }
        }
        
        viewModel.error.bind { [weak self] error in
            guard let error = error else { return }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self?.stopSpinnerView()
                self?.alert(message: error.errorDescription().localized,
                            title: "alert.generic.error".localized,
                            retryHandler: { _ in
                    self?.viewModel.fetchBeers()
                })
            }
        }
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "searchbar.title".localized
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        
        tableView.dataSource = self
        tableView.backgroundColor = UIColor(named: "backgroundLight")
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                       object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification) }
        notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                       object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification) }
        
    }
    
    func handleKeyboard(notification: Notification) {
        self.view.layoutIfNeeded()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showBeerDetails",
              let indexPath = tableView.indexPathForSelectedRow,
              let beer = viewModel.filteredData.value?[indexPath.row],
              let detailViewController = segue.destination as? BeerDetailViewController else {
                  return
              }
        
        detailViewController.beer = beer
    }
    
    func startSpinnerView() {
        addChild(spinnerController)
        spinnerController.view.frame = view.frame
        view.addSubview(spinnerController.view)
        spinnerController.didMove(toParent: self)
    }
    
    func stopSpinnerView() {
        spinnerController.willMove(toParent: nil)
        spinnerController.view.removeFromSuperview()
        spinnerController.removeFromParent()
    }
}

extension BeersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.filteredData.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BeerCell", for: indexPath) as? BeerCellView else {
            return UITableViewCell()
        }
        
        guard let beer = viewModel.filteredData.value?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor(named: "backgroundLight") : UIColor(named: "backgroundHeavy")
        cell.beerName.text = beer.name
        cell.beerDescription.text = beer.description
        cell.beerABV.text = beer.abv?.labelString.appending("%") ?? "not.found.value".localized
        cell.beerIBU.text = beer.ibu?.labelString ?? "not.found.value".localized
        cell.beerImage.kf.setImage(with: URL(string: beer.image_url))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}

extension BeersViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        if let text = searchBar.text {
            viewModel.filterBeers(text)
        }
    }
}


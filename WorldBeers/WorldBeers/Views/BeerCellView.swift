//
//  BeerCellViewController.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 27/01/22.
//

import Foundation
import UIKit

class BeerCellView: UITableViewCell {
    
    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var beerName: UILabel!
    @IBOutlet weak var beerDescription: UILabel!
    @IBOutlet weak var beerIBU: UILabel!
    @IBOutlet weak var beerABV: UILabel!
    
    override func prepareForReuse() {
        beerName.text = ""
        beerDescription.text = ""
        beerIBU.text = ""
        beerABV.text = ""
        beerImage.image = UIImage()
    }
    
}

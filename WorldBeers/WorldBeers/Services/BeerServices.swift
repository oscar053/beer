//
//  BeerServices.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 26/01/22.
//

import Foundation

enum BeerMapError: Error, LocalizedError {
    case invalidResponse
    case noData
    case failedRequest
    case invalidData
    
    func errorDescription() -> String {
        switch self {
        case .invalidResponse:
            return "error.invalid.response"
        case .noData:
            return "error.no.data"
        case .failedRequest:
            return "error.failed.request"
        case .invalidData:
            return "error.invalid.data"
        }
    }
}

class BeerServices {
    typealias BeerServicesCompletion = ([Beer]?, BeerMapError?) -> ()
    
    static func getBeers(completion: @escaping BeerServicesCompletion) {
        let service = HTTPServices()
        let environment = HTTPServices.HTTPServicesUrls.baseURL
        
        service.makeRequest(toEndpoint: .getBeers, environment: environment) { result in
            guard result.error == nil else {
                print("Failed request from \(environment): \(result.error!.localizedDescription)")
                completion(nil, .failedRequest)
                return
            }
            
            guard let data = result.data else {
                print("No data returned from \(environment)")
                completion(nil, .noData)
                return
            }
            
            guard let response = result.response else {
                print("Unable to process \(environment) response")
                completion(nil, .invalidResponse)
                return
            }
            
            guard response.httpStatusCode == 200 else {
                print("Failure response from \(environment): \(response.httpStatusCode)")
                completion(nil, .failedRequest)
                return
            }
            
            do {
                let beers = try JSONDecoder().decode([Beer].self, from: data)
                completion(beers, nil)
            } catch {
                print("Unable to decode \(environment) response: \(error.localizedDescription)")
                completion(nil, .invalidData)
            }
        }
    }
}

//
//  HTTPServices.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 26/01/22.
//

import Foundation
import Reqres

class HTTPServices: NSObject {
    
    enum HTTPServicesName {
        
        case getBeers
        
        var endpoint: String {
            switch self {
            case .getBeers:
                return "beers"
            }
        }
        
        func stringValue() -> String {
            return self.endpoint
        }
        
        var headers: RestEntity {
            var headers = RestEntity()
            
            switch self {
            case .getBeers:
                headers.add(value: "application/json", forKey: "Accept")
                headers.add(value: "application/json", forKey: "Content-Type")
            }
            
            return headers
        }
        
        var method: HttpMethod {
            switch self {
            case .getBeers:
                return .get            }
        }
    }
    
    enum HTTPServicesUrls: String {
        case baseURL = "https://api.punkapi.com/v2/"
        
        func url(withKey: Keys? = nil, withEndPoint: HTTPServicesName? = nil) -> URL? {
            if let key = withKey {
                let value = key.stringValue()
                return URL(string: self.rawValue + value)
            }
            
            if let apiName = withEndPoint {
                let value = apiName.stringValue()
                return URL(string: self.rawValue + value)
            }
            
            return URL(string: self.rawValue)
        }
    }
    
    enum Keys: String {
        case webKey = ""
        
        func stringValue() -> String{
            return self.rawValue
        }
    }
    
    private var requestHttpHeaders = RestEntity()
    
    var urlQueryParameters = RestEntity()
    
    private var httpBodyParameters = RestEntity()
    
    var httpBody: Data?
    
    // MARK: Public Methods
    
    func makeRequest(toEndpoint endpoint: HTTPServicesName,
                     key: Keys? = nil,
                     environment: HTTPServicesUrls,
                     body: RestEntity? = nil,
                     completion: @escaping (_ result: Results) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard let endpointURL = environment.url(withKey: key, withEndPoint: endpoint) else {
                completion(Results(withError: CustomError.failedToCreateUrl))
                return
            }
            
            self.httpBodyParameters = body ?? RestEntity()
            self.requestHttpHeaders = endpoint.headers
            let targetURL = self.addURLQueryParameters(toURL: endpointURL)
            let httpBody = self.getHttpBody()
            
            guard let request = self.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: endpoint.method) else
            {
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
            
            let sessionConfiguration = Reqres.defaultSessionConfiguration()
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data,
                                   response: Response(fromURLResponse: response),
                                   error: error))
            }
            task.resume()
        }
    }
    
    func makeRequestWithUserAgent(toEndpoint endpoint: HTTPServicesName,
                                  key: Keys? = nil,
                                  environment: HTTPServicesUrls = .baseURL,
                                  body: RestEntity? = nil,
                                  completion: @escaping (_ result: Results) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            guard let endpointURL = environment.url(withKey: key, withEndPoint: endpoint) else {
                completion(Results(withError: CustomError.failedToCreateUrl))
                return
            }
            
            self.httpBodyParameters = body ?? RestEntity()
            self.requestHttpHeaders = endpoint.headers
            
            let worldbeers = "worldbeers/" + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String)
            let iosVerion = "iOS " + UIDevice.current.systemVersion
            let locale = self.getLanguageCode()
            let screenSize = UIScreen.main.bounds.width
            let userAgent = "\(worldbeers) (iPhone; \(screenSize); \(iosVerion); \(locale))"
            
            self.requestHttpHeaders.add(value: userAgent, forKey: "User-Agent")
            let targetURL = self.addURLQueryParameters(toURL: endpointURL)
            let httpBody = self.getHttpBodySendParameters()
            
            guard let request = self.prepareRequest(withURL: targetURL, httpBody: httpBody, httpMethod: .post) else
            {
                completion(Results(withError: CustomError.failedToCreateRequest))
                return
            }
            
            let sessionConfiguration = Reqres.defaultSessionConfiguration()
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: request) { (data, response, error) in
                completion(Results(withData: data,
                                   response: Response(fromURLResponse: response),
                                   error: error))
            }
            task.resume()
        }
    }
    
    func getData(fromURL url: URL, completion: @escaping (_ data: Data?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, _, _) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
    
    // MARK: Private Methods
    
    private func addURLQueryParameters(toURL url: URL) -> URL {
        if urlQueryParameters.totalItems() > 0 {
            guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else { return url }
            var queryItems = [URLQueryItem]()
            for (key, value) in urlQueryParameters.allValues() {
                if let stringValue = value as? String {
                    let item = URLQueryItem(name: key, value: stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
                    queryItems.append(item)
                }
            }
            urlComponents.queryItems = queryItems
            guard let updatedURL = urlComponents.url else { return url }
            return updatedURL
        }
        return url
    }
    
    private func getHttpBody() -> Data? {
        guard let contentType = requestHttpHeaders.value(forKey: "Content-Type") as? String else { return nil }
        
        if contentType.contains("application/json") {
            return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [])
        } else if contentType.contains("application/x-www-form-urlencoded") {
            let bodyString = httpBodyParameters.allValues().map { key, value in
                "\(key)=\(String(describing: (value as! String)))"
            }.joined(separator: "&")
            return bodyString.data(using: .utf8)
        } else {
            return httpBody
        }
    }
    
    private func getHttpBodySendParameters() -> Data? {
        if let contentType = requestHttpHeaders.value(forKey: "Content-Type") as? String {
            if contentType.contains("application/json") {
                return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [])
            } else if contentType.contains("application/x-www-form-urlencoded") {
                let bodyString = httpBodyParameters.allValues().map { key, value in
                    "\(key)=\(String(describing: (value as! String)))"
                }.joined(separator: "&")
                return bodyString.data(using: .utf8)
            }
        }
        
        requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
        return try? JSONSerialization.data(withJSONObject: httpBodyParameters.allValues(), options: [])
        
    }
    
    private func prepareRequest(withURL url: URL?, httpBody: Data?, httpMethod: HttpMethod) -> URLRequest? {
        guard let url = url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        for (header, value) in requestHttpHeaders.allValues() {
            request.setValue(value as? String, forHTTPHeaderField: header)
        }
        
        /*
         HTTP request bodies are theoretically allowed for all methods except TRACE, however they are not commonly used except in PUT, POST and PATCH. Because of this, they may not be supported properly by some client frameworks, and you should not allow request bodies for GET, DELETE, TRACE, OPTIONS and HEAD methods.
         */
        switch httpMethod {
        case .post,
                .put,
                .patch:
            request.httpBody = httpBody
        default:
            break
        }
        
        return request
    }
}

extension HTTPServices {
    enum HttpMethod: String {
        case get
        case post
        case put
        case patch
        case delete
    }
    
    struct RestEntity {
        private var values: [String: Any] = [:]
        
        mutating func add(value: Any, forKey key: String) {
            values[key] = value
        }
        
        func value(forKey key: String) -> Any? {
            return values[key]
        }
        
        func allValues() -> [String: Any] {
            return values
        }
        
        func totalItems() -> Int {
            return values.count
        }
    }
    
    struct Response {
        var response: URLResponse?
        var httpStatusCode: Int = 0
        var headers = RestEntity()
        
        init(fromURLResponse response: URLResponse?) {
            guard let response = response else { return }
            self.response = response
            httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
            
            if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
                for (key, value) in headerFields {
                    headers.add(value: "\(value)", forKey: "\(key)")
                }
            }
        }
    }
    
    struct Results {
        var data: Data?
        var response: Response?
        var error: Error?
        
        init(withData data: Data?, response: Response?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
        }
        
        init(withError error: Error) {
            self.error = error
        }
    }
    
    enum CustomError: Error {
        case failedToCreateRequest,
             failedToCreateUrl
    }
}

// MARK: - Custom Error Description
extension HTTPServices.CustomError: LocalizedError {
    public var localizedDescription: String {
        switch self {
        case .failedToCreateRequest:
            return NSLocalizedString("Unable to create the URLRequest object", comment: "")
        case .failedToCreateUrl:
            return NSLocalizedString("Unable to creat the URL string", comment: "")
        }
    }
}

// MARK: - GET Image
extension HTTPServices {
    func getDeviceImage(fromUrl url: URL, completion: @escaping (Data?) -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            let sessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url, completionHandler: { (data, _, _) in
                guard let data = data else { completion(nil); return }
                completion(data)
            })
            task.resume()
        }
    }
    
    func getLanguageCode() -> String {
        return Locale.current.identifier
    }
}

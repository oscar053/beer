//
//  Beer.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 26/01/22.
//

import Foundation

struct Beer: Decodable {
    
    //MARK: - JSON Decodables
    
    let id: Int
    let name: String
    let image_url: String
    let description: String
    let abv: Float?
    let ibu: Float?
    let first_brewed: String
    let food_pairing: [String]
    let brewers_tips: String
    
}

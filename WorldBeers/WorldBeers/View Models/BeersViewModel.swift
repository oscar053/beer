//
//  BeersViewModel.swift
//  WorldBeers
//
//  Created by Emanuele Chiavelli on 26/01/22.
//

import Foundation

public class BeersViewModel {
    let data: Box<[Beer]?> = Box(nil)
    let filteredData: Box<[Beer]?> = Box(nil)
    let error: Box<BeerMapError?> = Box(nil)
    
    init() {
        fetchBeers()
    }
    
    func fetchBeers() {
        BeerServices.getBeers { [weak self] (beers, error) in
            guard let self = self,
                  let beers = beers else { self?.error.value = error
                                            return }
            self.data.value = beers
            self.filterBeers()
        }
    }
    
    func filterBeers(_ text: String = "") {
        guard let beers = self.data.value else {
            self.filteredData.value = []
            return
        }
        
        if text.count > 0 {
            let result = beers.filter { (beer: Beer) -> Bool in
                return beer.name.lowercased().contains(text.lowercased()) ||
                        beer.description.lowercased().contains(text.lowercased())
            }
            self.filteredData.value = result
        } else {
            self.filteredData.value = beers
        }
        
    }
}
